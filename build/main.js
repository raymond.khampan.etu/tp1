"use strict";

var data = [{
  name: 'Regina',
  base: 'tomate',
  price_small: 6.5,
  price_large: 9.95,
  image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
}, {
  name: 'Napolitaine',
  base: 'tomate',
  price_small: 6.5,
  price_large: 8.95,
  image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
}, {
  name: 'Spicy',
  base: 'crème',
  price_small: 5.5,
  price_large: 8,
  image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300'
}]; // data.sort( function(a, b)  {
//  return a.name.localeCompare(b.name);
// });
// data.sort( function(a, b)  {
//     return a.price_small-b.price_small;
//    });

data.sort(function (a, b) {
  return a.price_large - b.price_large;
});
var html = ""; // for(let i=0;i<data.length;i++){
//     const url= "images/"+data[i].toLowerCase()+".jpg";
//     html = html + `<article class="pizzaThumbnail"> <a href="${url}"><img src="${url}" /><section>${data[i].toLowerCase()}</section></a></article>`;
// }
// let url="";
// data.forEach( element =>{
//     url= "images/"+element.toLowerCase()+".jpg";
//     html = html + `<article class="pizzaThumbnail"> <a href="${url}"><img src="${url}" /><section>${element}</section></a></article>`;
// });

data // .filter(pizzas => pizzas.base == 'tomate')
// .filter(pizzas => pizzas.price_small < 6.0)
.filter(function (pizzas) {
  return pizzas.name.match(/i/g).length == 2;
}).forEach(function (element) {
  html += "<article class=\"pizzaThumbnail\">\n\t<a href=\"".concat(element.image, "\">\n\t\t<img src=\"").concat(element.image, "\" />\n\t\t<section>\n\t\t\t<h4>").concat(element.name, "</h4>\n\t\t\t<ul>\n\t\t\t\t<li>").concat(element.price_small, "</li>\n\t\t\t\t<li>").concat(element.price_large, "</li>\n\t\t\t</ul>\n\t\t</section>\n\t</a>\n</article>");
});
console.log(html);
document.querySelector('.pageContent').innerHTML = html;
//# sourceMappingURL=main.js.map